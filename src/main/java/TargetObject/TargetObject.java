package TargetObject;

/**
 * 目标接口，暴露给调用者的接口
 *
 * @author tengqingya
 * @create 2017-01-17 15:32
 */
public interface TargetObject {
    void doTartgetMethod();
}
