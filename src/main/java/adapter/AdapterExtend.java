package adapter;

import TargetObject.TargetObject;
import adaptee.Adaptee;

/**
 * 继承方式的适配器
 * 适配器不需要持有被适配的类的引用
 * 因为已经继承了，自然持有其引用
 *
 * @author tengqingya
 * @create 2017-01-17 16:06
 */
public class AdapterExtend extends Adaptee implements TargetObject{
    public void doTartgetMethod() {
        this.adapteeMethod("extends Adaptee implements TargetObject");
    }
}
