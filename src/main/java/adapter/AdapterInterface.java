package adapter;

import TargetObject.TargetObject;
import adaptee.AdapteeInterface;

/**
 * 实现adapter和target接口的方式
 *
 * @author tengqingya
 * @create 2017-01-18 11:16
 */
public class AdapterInterface implements AdapteeInterface,TargetObject {

    public void adapteeMethod( String name ) {
        System.out.print("adapteeMethod-->>"+name);
    }

    public void doTartgetMethod() {
        this.adapteeMethod("implements AdapteeInterface,TargetObject");
    }
}
