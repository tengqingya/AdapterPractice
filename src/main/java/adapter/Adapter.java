package adapter;

import TargetObject.TargetObject;
import adaptee.Adaptee;
import adaptee.AdapteeInterface;

/**
 * 适配器
 * 适配需要调用的接口
 * 适配器需要持有被适配的类的引用
 * @author tengqingya
 * @create 2017-01-17 15:43
 */
public class Adapter implements TargetObject {
    private AdapteeInterface adapteeInterface;

    public Adapter( AdapteeInterface adapteeInterface ) {
        this.adapteeInterface = adapteeInterface;
    }

    public void doTartgetMethod() {
        adapteeInterface.adapteeMethod("implements TargetObject");
    }
}
