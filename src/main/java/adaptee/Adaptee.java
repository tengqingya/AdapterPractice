package adaptee;

/**
 * 被适配的类
 * 实际被调用的接口所在类
 * @author tengqingya
 * @create 2017-01-17 15:38
 */
public class Adaptee implements AdapteeInterface{
    public void adapteeMethod(String name){
        System.out.println("adapteeMethod-->>"+name);
    }
}
