package adaptee;

/**
 * 被适配的基类接口
 *
 * @author tengqingya
 * @create 2017-01-18 9:26
 */
public interface AdapteeInterface {
    void adapteeMethod(String name);
}
