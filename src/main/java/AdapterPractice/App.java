package AdapterPractice;

import TargetObject.TargetObject;
import adaptee.Adaptee;
import adaptee.AdapteeInterface;
import adapter.Adapter;
import adapter.AdapterExtend;
import adapter.AdapterInterface;

/**
 * Hello world!
 */
public class App {
    private  TargetObject targetObject;

    public App( TargetObject targetObject ) {
        this.targetObject = targetObject;
    }

    /**
     * 通过接口调用方法
     * 实际调用的方法根据适配器
     */
    public void doSth(){
        targetObject.doTartgetMethod();
    }

    public static void main( String[] args ) {
        AdapteeInterface adaptee =new Adaptee();
        TargetObject targetObject =new Adapter(adaptee);
        App app = new App(targetObject);
        app.doSth();

        targetObject = new AdapterExtend();
        app = new App(targetObject);
        app.doSth();

        targetObject = new AdapterInterface();
        app = new App(targetObject);
        app.doSth();
    }
}
